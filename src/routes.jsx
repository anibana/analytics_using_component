import React from 'react';
import {Route} from 'react-router-dom';

import {providerConfig, analyticsConfig} from './analyticsConfig';
import {createAnalyticsProvider} from './analytics/createAnalyticsProvider';
import Analytics from './analytics/Analytics';

import Account from './pages/Account';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Flight from './pages/Flight';

const provider = createAnalyticsProvider(providerConfig);

const Routes = () => (
  <Analytics provider={provider} analyticsConfig={analyticsConfig}>
    <Route path='/home' component={Home} />
    <Route path='/dashboard' component={Dashboard} />
    <Route path='/account' component={Account} />
    <Route path='/flight/:number' component={Flight} />
  </Analytics>
);

export default Routes;
