import React from 'react';
import {withAnalytics} from '../analytics/withAnalytics';

const Account = ({track}) => {
  track({test: 'testing'});

  return <div>This is Account page</div>;
}

export default withAnalytics(Account);
