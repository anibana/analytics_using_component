import React, { Component } from 'react';
import { connect } from 'react-redux';
import {UPDATE_USER} from '../actions/action-types';

class Home extends Component {
  componentDidMount() {
    // this.props.fetchData();
  }

  render() {
    const {userId, name, gender} = this.props;
    return (
      <div>
        <div>
          <label>ID: </label>
          <span>{userId}</span>
        </div>
        <div>
          <label>Name: </label>
          <span>{name}</span>
        </div>
        <div>
          <label>Gender: </label>
          <span>{gender}</span>
        </div>
        <button onClick={() => this.props.fetchData()}> CLICK ME </button>
      </div>
    )
  }
}

const mapStateToProps = ({ user }) => ({
  name: user.name,
  userId: user.userId,
  gender: user.gender
});

const fetchData = () => {
  console.log('FETCHING DATA');
  return {type: UPDATE_USER};
};

const mapDispatchToProps = ({ fetchData })

export default connect(mapStateToProps, mapDispatchToProps)(Home);
