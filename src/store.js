import {createStore} from 'redux';
import reducers from './reducers';

//Mock initial store
const user = {
  userId: 'random-identifier',
  name: 'Jon Snow',
  gender: 'Male'
};

export default createStore(reducers, {user});
