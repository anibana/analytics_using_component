export const providerConfig = {
  id: 'PTFUI-123',
  applicationName: 'PTFUI',
  key: 'ABCD',
};

const selectUserDetails = ({user}) => ({
  name: user.name,
  gender: user.gender,
  userId: user.userId
});

export const analyticsConfig = {
  '/home': {
    value: 100,
    name: 'home',
    some: {
      key: {
        a: 1,
        details: selectUserDetails
      }
    }
  },
  '/dashboard': {
    value: 200,
    name: 'dashboard',
    a: { b: {c: () => ({test: '1'}) }},
    attr: 'some-random-data'
  },
  '/account': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  '/flight/:number': {
    value: 400,
    name: 'flight',
    attr: 'another random data',
  },
  default: {
    value: 10
  }
};
