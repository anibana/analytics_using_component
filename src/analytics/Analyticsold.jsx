import {Component} from 'react';
import PropTypes from 'prop-types';
import {withContext} from 'recompose';

import {constructEvent} from './utils';

export const analyticsWithWindow = (window) => {
  // const Analytics = ({provider, analyticsConfig, children}, {store}) => {
  //   const {location, navigator: {userAgent}} = window;
  //   const event = constructEvent({location, userAgent, analyticsConfig, store});

  //   provider.track(event);

  //   return children;
  // };


  // Analytics.propTypes = {
  //   provider: PropTypes.object.isRequired,
  //   analyticsConfig: PropTypes.object.isRequired
  // };

  // Analytics.contextTypes = {
  //   store: PropTypes.object.isRequired
  // };

  // Analytics.childContextTypes = {
  //   track: PropTypes.func
  // };

  // Analytics.getChildContext = function() {
  //   return {track: this.props.provider.track};
  // }

  // return Analytics;
  class Analytics extends Component {
    getChildContext() {
      return {track: this.props.provider.track};
    }

    render() {
      const {provider, analyticsConfig, children} = this.props;
      const {location, navigator: {userAgent}} = window;
      const event = constructEvent({location, userAgent, analyticsConfig, store: this.context.store});
  
      provider.track(event);
  
      return children;
    };
  }

  Analytics.propTypes = {
    provider: PropTypes.object.isRequired,
    analyticsConfig: PropTypes.object.isRequired
  };

  Analytics.contextTypes = {
    store: PropTypes.object.isRequired
  };

  Analytics.childContextTypes = {
    track: PropTypes.func
  };

  return Analytics;
  // return withContext(
  //   {track: PropTypes.func},
  //   ({provider}) => ({track: provider.track})
  // )(Analytics);
};


export default analyticsWithWindow(global.window);
