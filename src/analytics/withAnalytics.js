import PropTypes from 'prop-types';
import {getContext} from 'recompose';

export const withAnalytics = (Component) => getContext({
  track: PropTypes.func
})(Component);
