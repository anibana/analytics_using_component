import PropTypes from 'prop-types';
import {compose, setPropTypes, withProps, withContext, getContext} from 'recompose';

import {constructEvent} from './utils';

export const Analytics = ({location, navigator, provider, analyticsConfig, children, store}) => {
  const {userAgent} = navigator;
  const event = constructEvent({location, userAgent, analyticsConfig, store});

  provider.track(event);

  return children;
};

// enhancer :: Window -> HOC
const enhancer = ({location, navigator}) => compose(
  withProps({location, navigator}),
  withContext(
    {track: PropTypes.func},
    ({provider}) => ({track: provider.track})
  ),
  getContext({store: PropTypes.object.isRequired}),
  setPropTypes({
    location: PropTypes.object.isRequired,
    navigator: PropTypes.object.isRequired,
    store: PropTypes.object.isRequired,
    provider: PropTypes.object.isRequired,
    analyticsConfig: PropTypes.object.isRequired,
    children: PropTypes.node
  })
);

export default enhancer(global.window)(Analytics);
