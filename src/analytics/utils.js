import {matchPath} from 'react-router';

import {
  find,
  pairs,
  last,
  reduce,
  replace,
  isFunction,
  isObject
} from '../fp';

const getPathConfig = ({pathname, hash}, analyticsConfig) => {
  const path = (hash && replace('#', '', hash)) || pathname;

  const matchedConfig = find(([key]) => {
    const matchedPath = matchPath(path, {path: key, exact: true});
    return matchedPath && matchedPath.isExact;
  }, pairs(analyticsConfig));

  return matchedConfig && last(matchedConfig) || analyticsConfig.default;
};

const eventReducer = (config, state) => (
  reduce((acc, [k, v]) => {
    if (isObject(v)) {
      return {
        ...acc,
        [k]: eventReducer(v, state)
      };
    }

    if (isFunction(v)) {
      return {
        ...acc,
        [k]: v(state)
      };
    }

    return {...acc, [k]: v};
  }, {}, pairs(config))
);

export const constructEvent = ({location, userAgent, analyticsConfig, store}) => {
  const baseConfig = {url: location.href, userAgent};
  const pathConfig = getPathConfig(location, analyticsConfig);

  return {
    ...eventReducer(pathConfig, store.getState()),
    ...baseConfig
  };
};
