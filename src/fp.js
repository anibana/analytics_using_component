import {type} from 'ramda';

export {
  find,
  last,
  reduce,
  replace,
  toPairs as pairs,
} from 'ramda';

export const isFunction = (o) => type(o) === 'Function';

export const isObject = (o) => type(o) === 'Object';
