import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom';
import Navbar from './components/navbar';
import Routes from './routes';

const App = () => (
  <Router>
    <div>
      <Navbar />
      <Routes />
    </div>
  </Router>
);

export default App;
