import {FETCH_USER, UPDATE_USER} from '../actions/action-types';
const defaultState = () => ({});

const userReducer = (state = defaultState(), action) => {
  switch (action.type) {
    case FETCH_USER:
      return {
        ...state,
        userId: 'random-identifier',
        name: 'Jon Snow',
        gender: 'Male'
      }
    case UPDATE_USER:
      return {
        ...state,
        userId: 'random-identifier-1',
        name: 'Aga Lis',
        gender: 'Female'
      }
    default:
      return state;
  };
};

export default userReducer;
