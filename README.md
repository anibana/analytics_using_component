# Route listener using Route Component Wrapper

## Run in local

- yarn install
- yarn start

**Note:** *This is just an experiment on how the route listener will looks like. For now, Analytics provider will just console.log the events. Later we will replace it with an actual API call.*

**Another Note**: *analytics folder will be extracted to a node module. For now, for demonstration it is part of the React application*

## Sample Configuration

```javascript
export const provider = {
  id: 'PTFUI-123',
  applicationName: 'PTFUI',
  key: 'ABCD'
};

export const pageTransition = {
  '/home': {
    value: 100,
    name: 'home',
  },
  '/dashboard': {
    value: 200,
    name: 'dashboard',
    attr: 'some-random-data'
  },
  '/account': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  '/flight/:number': {
    value: 300,
    name: 'account',
    attr: 'another random data'
  },
  default: {
    value: 10
  }
};

```

### Sample Usage
```javascript
import React from 'react';
import {Route} from 'react-router-dom';

import {provider, pageTransition} from './analyticsConfig';
import {createAnalyticsProvider} from './analytics/createAnalyticsProvider';
import Analytics from './analytics/Analytics';

import Account from './pages/Account';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import Flight from './pages/Flight';

const analyticsProvider = createAnalyticsProvider(provider);

const Routes = () => (
  <Analytics analyticsProvider={analyticsProvider} pageTransitionConfig={pageTransition}>
    <Route path='/home' component={Home} />
    <Route path='/dashboard' component={Dashboard} />
    <Route path='/account' component={Account} />
    <Route path='/flight/:number' component={Flight} />
  </Analytics>
);

export default Routes;
```

### Sample output
```javascript
{path: "/dashboard", value: 200, name: "dashboard", attr: "some-random-data", applicationName: "PTFUI", id: "PTFUI-123"}
```

### PROS
- single route page tracker config
- easy to install to react app, just need to wrap your Routes with
  Analytics component (component-based solution!)
- can read dynamic data from store since the Analytics component itself
  is subscribed to store.
- Very testable. you don't have to run the whole application just to test the
  analytics functionality.

### CONS
- can't think of anything right now. it feels natural to develop using
  this approach. :)
